package sample;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {
    private static Logger logger = LoggerFactory.getLogger(Main.class);//sample.Main

    // OR
    // Logger logger = LoggerFactory.getLogger("com.howtodoinjava.demo");
    public static void main(final String[] args) {


        logger.info("Info Message Logged !!!");
        String article = "ART";
        logger.error("Article fecthed for id : {} is : {}", 1, article);
        logger.info("Article fecthed for id : " + 1 + " is : " + article + "");
        while (true){
            logger.error("something went wrong");
        }


    }
}