package sample;


import java.io.IOException;
import java.io.InputStream;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class MyClass {
    private static Logger LOGGER = null;

    static {
        InputStream stream = MyClass.class.getClassLoader().
                getResourceAsStream("logging.properties");
        LogManager logManager = LogManager.getLogManager();
        try {
            logManager.readConfiguration(stream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        LOGGER= Logger.getLogger(MyClass.class.getName());

    }

    public static void main(String[] args) {
        System.out.println("-- main method starts --");
        LOGGER.info("in MyClass");
        LOGGER.warning("a test warning");
    }
}