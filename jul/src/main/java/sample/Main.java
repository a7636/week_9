package sample;

import java.util.Date;
import java.util.logging.*;

public class Main {
    private static final Logger LOGGER = Logger.getLogger(Main.class.getName());//sample.Main

    static {

        ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new SimpleFormatter() {
            private static final String format = "[%1$tF %1$tT] [%2$-7s] %3$s %n";

            @Override
            public synchronized String format(LogRecord lr) {
                return String.format(format,
                        new Date(lr.getMillis()),
                        lr.getLevel().getLocalizedName(),
                        lr.getMessage()
                );
            }
        });
        handler.setLevel(Level.ALL);
//        handler.setFormatter(new XMLFormatter());
        LOGGER.setUseParentHandlers(false);
        LOGGER.addHandler(handler);

    }

    public static void main(String[] args) {

//        LOGGER.setLevel(Level.ALL);

        LOGGER.log(Level.INFO, "information message");
        LOGGER.info("information message");

        LOGGER.fine("fine message");
        LOGGER.log(Level.FINE, "fine message");

        LOGGER.severe("error msg");

        LOGGER.warning("warning");

        LogRecord logRecord = new LogRecord(Level.INFO, "log");
        LOGGER.log(logRecord);


        LOGGER.severe("Error: ");

        System.err.println("error info");
        System.out.println("info");

        someMethod();
    }

    static void someMethod(){
        LOGGER.info("from method");
    }



}
