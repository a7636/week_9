package sample;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {
    private static final Logger logger = LogManager.getLogger(Main.class);//sample.Main

    public static void main(String[] args) {
        logger.error("Something went wrong");
        logger.log(Level.INFO, "message");
        logger.fatal("hhh");
        logger.debug("rrrr");
        logger.trace("trace");


        while (true) {
            logger.debug("Hello from Log4j 2");
        }



    }



}
